# FRC IntelliJ IDEA Plugin

An IntelliJ IDEA plugin for FIRST Robotics Competition (FRC) robot development in Java. This plugin works with either the free open-sourced Community Edition or the licensed Ultimate Edition of IntelliJ IDEA.


## Current State
**This plug-in is in beta and is not feature complete yet.** I started developing and using this plug-in for my own use late in the 2016 FRC build season. I had hoped to have it more feature rich prior to the 2017 FRC season. But time did not allow for such. Nevertheless, the preliminary features avaiable in the plugin make it very useful. Now that the 2017 build season has ended, I hope to he able to spend more time on enhancing the plugin. I have many featured planned. Thanks.

:warning: **If using IntelliJ IDEA v2017.1, be sure you are using v0.5 or later of the plugin to prevent issues, specifically Issue #3.** Otherwise the plugin requires IntelliJ IDEA v2016.3.x or later.


## Documentation

See the project's **[wiki page](https://gitlab.com/Javaru/frc-intellij-idea-plugin/wikis/home)** for documentation on using this plugin.
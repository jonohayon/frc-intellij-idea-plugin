/*
 * Copyright 2015-2016 Mark Vedder
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package net.javaru.iip.frc.actions.create.ui.forms;

import java.awt.*;
import javax.swing.*;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.project.Project;
import com.intellij.ui.components.JBLabel;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.util.ui.UIUtil.FontColor;

import net.javaru.iip.frc.FrcIcons;
import net.javaru.iip.frc.actions.create.ui.ValidationPanelBean;
import net.javaru.iip.frc.ui.components.FrcIconComponent;



public class FrcWizardMasterPanel implements PanelBean
{
    private static final Logger LOG = Logger.getInstance(FrcWizardMasterPanel.class);

    private JPanel myRootPanel;
    private JPanel myHeaderPanel;
    private JPanel myCenterPanel;
    private FrcIconComponent myIcon;
    private JPanel myTitlePanel;
    private JBLabel myProductLabel;
    private JBLabel myTaskTitleLabel;


    public FrcWizardMasterPanel(@Nullable Project project,
                                @NotNull ModelPanelBean panelBean,
                                @NotNull String taskTitle,
                                @Nullable String taskDescription)
    {
        Icon icon = FrcIcons.FIRST_ICON_WIZARD_PANELS;
        $$$setupUI$$$();
        myIcon.setPreferredSize(new Dimension(icon.getIconWidth(), icon.getIconHeight()));
        myIcon.setIcon(icon);
        myTaskTitleLabel.setText(taskTitle);

        final JPanel contentPanel = panelBean.getRootPanel();
        JPanel panelToUse = taskDescription != null ? new DescriptionPanelBean(contentPanel, taskDescription).getRootPanel() : contentPanel;

        // TODO: create separate constructor?
        if (panelBean instanceof ValidatingModelPanelBean)
        {
            panelToUse = new ValidationPanelBean((ValidatingModelPanelBean) panelBean).getRootPanel();
        }

        myCenterPanel.add(panelToUse);
    }


    @NotNull
    @Override
    public JPanel getRootPanel() { return myRootPanel; }


    private void createUIComponents()
    {
        // place custom component creation code here 
    }


    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$()
    {
        myRootPanel = new JPanel();
        myRootPanel.setLayout(new BorderLayout(0, 15));
        myRootPanel.setMinimumSize(new Dimension(350, 200));
        myRootPanel.setPreferredSize(new Dimension(400, 300));
        myHeaderPanel = new JPanel();
        myHeaderPanel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
        myRootPanel.add(myHeaderPanel, BorderLayout.NORTH);
        myHeaderPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(20, 10, 20, 10), null));
        myIcon = new FrcIconComponent();
        myHeaderPanel.add(myIcon);
        myTitlePanel = new JPanel();
        myTitlePanel.setLayout(new GridLayoutManager(2, 1, new Insets(0, 0, 0, 0), -1, -1));
        myHeaderPanel.add(myTitlePanel);
        myProductLabel = new JBLabel();
        myProductLabel.setFont(new Font(myProductLabel.getFont().getName(), Font.ITALIC, myProductLabel.getFont().getSize()));
        myProductLabel.setText("FIRST Robotics Competition");
        myTitlePanel.add(myProductLabel,
                         new GridConstraints(1,
                                             0,
                                             1,
                                             1,
                                             GridConstraints.ANCHOR_WEST,
                                             GridConstraints.FILL_NONE,
                                             GridConstraints.SIZEPOLICY_WANT_GROW,
                                             GridConstraints.SIZEPOLICY_FIXED,
                                             null,
                                             null,
                                             null,
                                             0,
                                             false));
        myTaskTitleLabel = new JBLabel();
        myTaskTitleLabel.setFont(new Font(myTaskTitleLabel.getFont().getName(), Font.BOLD, 22));
        myTaskTitleLabel.setFontColor(FontColor.NORMAL);
        myTaskTitleLabel.setText("(Task Title)");
        myTaskTitleLabel.setToolTipText("");
        myTitlePanel.add(myTaskTitleLabel,
                         new GridConstraints(0,
                                             0,
                                             1,
                                             1,
                                             GridConstraints.ANCHOR_CENTER,
                                             GridConstraints.FILL_NONE,
                                             GridConstraints.SIZEPOLICY_WANT_GROW,
                                             GridConstraints.SIZEPOLICY_FIXED,
                                             null,
                                             null,
                                             null,
                                             0,
                                             false));
        myCenterPanel = new JPanel();
        myCenterPanel.setLayout(new BorderLayout(0, 0));
        myRootPanel.add(myCenterPanel, BorderLayout.CENTER);
        myCenterPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEmptyBorder(), null));
    }


    /** @noinspection ALL */
    public JComponent $$$getRootComponent$$$() { return myRootPanel; }
}
